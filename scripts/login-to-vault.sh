#!/usr/bin/env bash
set -euo pipefail

read -p 'VAULT username: ' vaultUser
read -s -p 'VAULT password: ' vaultPassword

printf "\n\n"

curl -s --request POST \
     --data "{\"password\": \"${vaultPassword}\"}" \
       ${VAULT_ADDR}/v1/auth/userpass/login/${vaultUser} | jq -r .auth.client_token
