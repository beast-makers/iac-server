#!/usr/bin/env bash
set -euxo pipefail

containerId=$(docker ps | grep customer-webportal | awk '{print $1}')
docker exec -it $containerId /bin/bash
