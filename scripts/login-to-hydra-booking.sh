#!/usr/bin/env bash
set -euxo pipefail

containerId=$(docker ps | grep hydra-booking | awk '{print $1}')
docker exec -it $containerId /bin/bash
