#!/usr/bin/env bash
set -euxo pipefail

sh -c " \
  cd /ansible \
  && ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook \
  -i /terraform/development/vault-server-service/inventory/vault-server-service-00-inventory.yml \
  --tags vault-open \
  vault-server-service.yml \
"
