#!/usr/bin/env bash
set -euxo pipefail

containerId=$(docker ps | grep beast-makers-api | awk '{print $1}')
docker exec -it $containerId /bin/bash
