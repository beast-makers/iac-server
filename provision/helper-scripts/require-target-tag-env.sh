#!/usr/bin/env bash
set -euo pipefail

TARGET_TAG=${TARGET_TAG:-}

if [[ -z "${TARGET_TAG}" ]]; then
  echo "TARGET_TAG environment variable is not set"
  exit 1
fi
