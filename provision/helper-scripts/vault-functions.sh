#!/usr/bin/env bash
set -euo pipefail

OWN_DIR=$(dirname $(readlink -f ${BASH_SOURCE[0]}))

source "${OWN_DIR}/vault-login.sh"

function vault_loadDockerAuthVariables () {
  local data=$(curl -s \
      --header "X-Vault-Token: ${VAULT_TOKEN}" \
      ${VAULT_ADDR}/v1/secret/testing/docker-registry)

  DOCKER_REPOSITORY=$(printf $data | jq -r .data.repository)
  DOCKER_PASSWORD=$(printf $data | jq -r .data.pass)
  DOCKER_USERNAME=$(printf $data | jq -r .data.user)
  DOCKER_SERVER=$(printf $data | jq -r .data.server)

  export DOCKER_REPOSITORY=$DOCKER_REPOSITORY
  export DOCKER_PASSWORD=$DOCKER_PASSWORD
  export DOCKER_USERNAME=$DOCKER_USERNAME
  export DOCKER_SERVER=$DOCKER_SERVER
}

function vault_loadHydraWebapiDatabaseCredentials () {
  local data=$(curl -s \
      --header "X-Vault-Token: ${VAULT_TOKEN}" \
      ${VAULT_ADDR}/v1/secret/testing/hydra-database)

  DB_WEBAPI_USER=$(printf $data | jq -r .data.user)
  DB_WEBAPI_PASSWORD=$(printf $data | jq -r .data.pass)

  export DB_WEBAPI_USER=$DB_WEBAPI_USER
  export DB_WEBAPI_PASSWORD=$DB_WEBAPI_PASSWORD
}

function vault_loadBeastMakersApiAccessToken () {
  local data=$(curl -s \
      --header "X-Vault-Token: ${VAULT_TOKEN}" \
      ${VAULT_ADDR}/v1/secret/testing/beast-makers-api-tokens)

  GIT_ACCESS_TOKEN=$(printf $data | jq -r .data.git_repo_token)

  export GIT_ACCESS_TOKEN=$GIT_ACCESS_TOKEN
}

function vault_loadCustomerWebPortalAccessToken () {
  local data=$(curl -s \
      --header "X-Vault-Token: ${VAULT_TOKEN}" \
      ${VAULT_ADDR}/v1/secret/testing/customer-webportal-tokens)

  GIT_ACCESS_TOKEN=$(printf $data | jq -r .data.git_repo_token)

  export GIT_ACCESS_TOKEN=$GIT_ACCESS_TOKEN
}

function vault_loadHydraBookingAccessToken () {
  local data=$(curl -s \
      --header "X-Vault-Token: ${VAULT_TOKEN}" \
      ${VAULT_ADDR}/v1/secret/testing/hydra-booking-tokens)

  GIT_ACCESS_TOKEN=$(printf $data | jq -r .data.git_repo_token)

  export GIT_ACCESS_TOKEN=$GIT_ACCESS_TOKEN
}

function vault_loadIacServerGitAccess () {
  local data=$(curl -s \
      --header "X-Vault-Token: ${VAULT_TOKEN}" \
      ${VAULT_ADDR}/v1/secret/testing/app-deployment-tokens)

  IAC_SERVER_DEPLOY_TOKEN=$(printf $data | jq -r .data.iac_server_deploy_token)
  IAC_SERVER_DEPLOY_USERNAME=$(printf $data | jq -r .data.iac_server_deploy_username)

  export IAC_SERVER_DEPLOY_TOKEN=$IAC_SERVER_DEPLOY_TOKEN
  export IAC_SERVER_DEPLOY_USERNAME=$IAC_SERVER_DEPLOY_USERNAME
}
