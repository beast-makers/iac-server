#!/usr/bin/env bash
set -euo pipefail

VAULT_USER=${VAULT_USER:-"developer"}
VAULT_USER_FILE="./${VAULT_USER}.json"
#VAULT_USER_FILE="/area-51/vault/users/${VAULT_USER}.json"

function vault_is_sealed() {
  local data=$(curl -s ${VAULT_ADDR}/v1/sys/health)
  local is_sealed=$(printf ${data} | jq .sealed)

  if [ $is_sealed == 'true' ]; then
    printf "\nVault is sealed. Use the /scripts/open-vault.sh\n"
    exit 1
  fi
}

function vault_login() {
  vault_is_sealed
  if [ -f "$VAULT_USER_FILE" ]; then
    vault_fromFileLogin
  else
    printf "\nCould not find '$VAULT_USER_FILE'. Will continue with interactive login\n"
    vault_interactiveLogin
  fi
}

function vault_interactiveLogin () {
  read -p 'VAULT username: ' vaultUser
  read -s -p 'VAULT password: ' vaultPassword

  vault_requestLoginToken $vaultUser $vaultPassword
}

function vault_fromFileLogin () {
  local data=$(cat ${VAULT_USER_FILE})

  local vaultUser=$VAULT_USER
  local vaultPassword=$(echo $data | jq -r .password)

  vault_requestLoginToken $vaultUser $vaultPassword
}

function vault_requestLoginToken () {
  local vaultUser=$1
  local vaultPassword=$2
  local vaultToken=$(curl -s --request POST \
     --data "{\"password\": \"${vaultPassword}\"}" \
       ${VAULT_ADDR}/v1/auth/userpass/login/${vaultUser} | jq -r .auth.client_token)

  VAULT_TOKEN=$vaultToken
}
