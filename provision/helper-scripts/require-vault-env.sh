#!/usr/bin/env bash
set -euo pipefail

VAULT_ADDR=${VAULT_ADDR:-}

if [[ -z "${VAULT_ADDR}" ]]; then
  echo "VAULT_ADDR environment variable is not set"
  exit 1
fi
