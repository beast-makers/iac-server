#!/usr/bin/env bash
set -euo pipefail

DOCKER_SOURCE_TAG=${DOCKER_SOURCE_TAG:-}
#DOCKER_SOURCE_TAG=now

if [[ -z "${DOCKER_SOURCE_TAG}" ]]; then
  echo "DOCKER_SOURCE_TAG environment variable is not set"
  exit 1
fi
