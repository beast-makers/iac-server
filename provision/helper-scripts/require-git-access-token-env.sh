#!/usr/bin/env bash
set -euo pipefail

GIT_ACCESS_TOKEN=${GIT_ACCESS_TOKEN:-}

if [[ -z "${GIT_ACCESS_TOKEN}" ]]; then
  echo "GIT_ACCESS_TOKEN environment variable is not set"
  exit 1
fi
