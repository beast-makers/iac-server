#!/usr/bin/env bash
set -euo pipefail

SOURCE_TAG=${SOURCE_TAG:-}

if [[ -z "${SOURCE_TAG}" ]]; then
  echo "SOURCE_TAG environment variable is not set"
  exit 1
fi
