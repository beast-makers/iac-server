#!/usr/bin/env bash
set -euo pipefail

function consul_fetchNomadServerConnection () {
  NOMAD_SERVER_IP=$(curl -s "${CONSUL_HTTP_ADDR}/v1/catalog/service/nomad-server?tag=consul" | jq -r .[0].Address)
  NOMAD_SERVER_PORT=$(curl -s "${CONSUL_HTTP_ADDR}/v1/catalog/service/nomad-server?tag=consul" | jq -r .[0].ServicePort)

  export NOMAD_SERVER_IP=$NOMAD_SERVER_IP
  export NOMAD_SERVER_PORT=$NOMAD_SERVER_PORT
}

function consul_fetchDBServerConnection () {
  DB_SERVER_IP=$(curl -s "${CONSUL_HTTP_ADDR}/v1/catalog/service/postgresql" | jq -r .[0].Address)
  DB_SERVER_PORT=$(curl -s "${CONSUL_HTTP_ADDR}/v1/catalog/service/postgresql" | jq -r .[0].ServicePort)

  export DB_SERVER_IP=$DB_SERVER_IP
  export DB_SERVER_PORT=$DB_SERVER_PORT
}
