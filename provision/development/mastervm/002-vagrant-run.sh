#!/usr/bin/env bash
set -euxo pipefail

vagrant ssh mastervm --command "sh -c \"cd /ansible && ansible-playbook -i inventory/iac-server.yml iac-server.yml\""

vagrant ssh mastervm --command "sh -c \"cd /packer/consul-server-service && ./build.sh\""
vagrant ssh mastervm --command "sh -c \"cd /terraform/development/consul-server-service && terraform init && ./do.sh apply -auto-approve\""

vagrant ssh mastervm --command "sh -c \"cd /packer/vault-server-service && ./build.sh\""
vagrant ssh mastervm --command "sh -c \"cd /terraform/development/vault-server-service && terraform init && ./do.sh apply -auto-approve\""

vagrant ssh mastervm --command "sh -c \"cd /packer/nomad-server-service && ./build.sh\""
vagrant ssh mastervm --command "sh -c \"cd /terraform/development/nomad-server-service && terraform init && ./do.sh apply -auto-approve\""

vagrant ssh mastervm --command "sh -c \"cd /terraform/development/nomad-client-service && terraform init && ./do.sh apply -auto-approve\""

vagrant ssh mastervm --command "sh -c \"cd /packer/haproxy-service && ./build.sh\""
vagrant ssh mastervm --command "sh -c \"cd /terraform/development/haproxy-service && terraform init && ./do.sh apply -auto-approve\""

vagrant ssh mastervm --command "sh -c \"cd /packer/database-service && ./build.sh\""
vagrant ssh mastervm --command "sh -c \"cd /terraform/development/database-master-service && terraform init && ./do.sh apply -auto-approve\""

vagrant ssh mastervm --command "sh -c \"cd /packer/webapi-redis && ./build.sh\""
vagrant ssh mastervm --command "sh -c \"cd /terraform/development/webapi-redis && terraform init && ./do.sh apply -auto-approve\""

vagrant ssh mastervm --command "sh -c \"cd /packer/webfe-redis && ./build.sh\""
vagrant ssh mastervm --command "sh -c \"cd /terraform/development/webfe-redis && terraform init && ./do.sh apply -auto-approve\""

vagrant ssh mastervm --command "sh -c \"cd /packer/local-dns && ./build.sh\""
vagrant ssh mastervm --command "sh -c \"cd /terraform/development/local-dns && terraform init && ./do.sh apply -auto-approve\""

#vagrant ssh iac --command "sh -c \"cd /terraform/development/hydra-webapi && terraform init && ./do.sh apply -auto-approve\""

#vagrant ssh iac --command "sh -c \"cd /terraform/development/hydra-webfe && terraform init && ./do.sh apply -auto-approve\""

