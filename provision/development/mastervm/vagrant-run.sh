#!/usr/bin/env bash

function installIptablesPersistent()
{
  sudo mkdir -p /etc/iptables/
  sudo cp /provision/development/mastervm/iptables_rules.v4 /etc/iptables/rules.v4

  echo iptables-persistent iptables-persistent/autosave_v4 boolean false | sudo debconf-set-selections
  echo iptables-persistent iptables-persistent/autosave_v6 boolean false | sudo debconf-set-selections

  sudo apt-get -y install iptables-persistent
  sudo apt-get install -y iptables-persistent
}

sudo apt-get update
if ! hash ansible 2>/dev/null; then
  sudo apt-get install -y python-pip lxc-dev jq sshpass
  sudo pip install ansible lxc-python2
  ansible-galaxy collection install ansible.posix
fi

installIptablesPersistent
