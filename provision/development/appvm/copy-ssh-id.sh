#!/usr/bin/env bash

ssh-keygen -b 2048 -t rsa -f /home/vagrant/.ssh/id_rsa -q -N ""
ssh-keyscan -H 192.168.100.10 >> ~/.ssh/known_hosts
sshpass -p "vagrant" ssh-copy-id -i ~/.ssh/id_rsa.pub vagrant@192.168.100.10
