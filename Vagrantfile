Vagrant.configure("2") do |config|
  config.vm.box = "generic/debian10"
  config.ssh.forward_agent = true

  config.vm.define "mastervm" do |mastervm|
    mastervm.vm.network "private_network", ip: "192.168.100.10"

    mastervm.vm.synced_folder ".", '/vagrant', disabled:true
    mastervm.vm.synced_folder "./provision", '/provision'
    mastervm.vm.synced_folder "./ansible", '/ansible'
    mastervm.vm.synced_folder "./packer", '/packer'
    mastervm.vm.synced_folder "./terraform", '/terraform'
    mastervm.vm.synced_folder "./nomad", '/nomad'
    mastervm.vm.synced_folder "./scripts", '/scripts'
    mastervm.vm.synced_folder "../area-51-dev", '/area-51'

    mastervm.vm.provider "virtualbox" do |v|
      v.cpus = 2
      v.memory = "2048" # that is 2 GB
    end

    mastervm.vm.provision :shell,
      keep_color: true,
      inline: 'cd /provision/development/mastervm && ./vagrant-run.sh'
  end

  config.vm.define "appvm" do |appvm|
    appvm.vm.network "private_network", ip: "192.168.100.11"

    appvm.vm.synced_folder ".", '/vagrant', disabled:true
    appvm.vm.synced_folder "./provision", '/provision'
    appvm.vm.synced_folder "./scripts", '/scripts'
    appvm.vm.synced_folder "../hydra-webapi", '/var/hydra-webapi'
    appvm.vm.synced_folder "../hydra-webfe", '/var/hydra-webfe'
    appvm.vm.synced_folder "../hydra-booking", '/var/hydra-booking'

    appvm.vm.provider "virtualbox" do |v|
      v.cpus = 2
      v.memory = "1536" # that is 1.5 GB
    end

    appvm.vm.provision :shell,
      keep_color: true,
      inline: 'cd /provision/development/appvm && ./vagrant-run.sh'
  end

  unless Vagrant.has_plugin?("vagrant-vbguest")
    puts 'Installing vagrant-vbguest Plugin...'
    system('vagrant plugin install vagrant-vbguest')
  end
  config.vbguest.auto_update = true

end
