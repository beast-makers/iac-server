#!/usr/bin/env bash
set -euxo pipefail

INPUT_TEMPLATE_FILE='/etc/nginx/sites-enabled.tmpl/_hydra-booking.conf.tmpl'
OUTPUT_CONFIG_FILE='/etc/nginx/sites-enabled/_hydra-booking.conf'

envsubst '${HYDRA_BOOKING_SEVER_NAME}' < ${INPUT_TEMPLATE_FILE} > ${OUTPUT_CONFIG_FILE}
supervisorctl start nginx
