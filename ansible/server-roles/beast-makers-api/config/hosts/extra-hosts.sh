#!/usr/bin/env bash
set -euo pipefail

APP_TYPE=${APP_TYPE:-}
if [[ -z "${APP_TYPE}" ]]; then
  echo "APP_TYPE environment variable is not set"
  exit 1
fi

APP_ENV=${APP_ENV:-}
if [[ -z "${APP_ENV}" ]]; then
  echo "APP_ENV environment variable is not set"
  exit 1
fi

SITES_CONFIG_FILE="/var/webapi/config/env/${APP_TYPE}/${APP_ENV}/sites.json"

sitesCount=$(cat ${SITES_CONFIG_FILE} | jq '.| length')
index=$(($sitesCount - 1))

while [[ $index -gt -1 ]]; do
  entry=$(cat ${SITES_CONFIG_FILE} | jq ".[${index}]")

  HOST=$(echo $entry | jq -r .host)
  cat /etc/hosts | grep ${HOST} || printf "127.0.0.1\t${HOST}\n" | tee -a /etc/hosts

  index=$((index - 1))
done
