#!/usr/bin/env bash
set -euo pipefail

APP_TYPE=${APP_TYPE:-}
if [[ -z "${APP_TYPE}" ]]; then
  echo "APP_TYPE environment variable is not set"
  exit 1
fi

APP_ENV=${APP_ENV:-}
if [[ -z "${APP_ENV}" ]]; then
  echo "APP_ENV environment variable is not set"
  exit 1
fi

INPUT_TEMPLATE_FILE='/etc/nginx/sites-enabled.tmpl/_webapi.conf.tmpl'
SITES_CONFIG_FILE="/var/webapi/config/env/${APP_TYPE}/${APP_ENV}/sites.json"
OUTPUT_CONFIG_DIR='/etc/nginx/sites-enabled/'

sitesCount=$(cat ${SITES_CONFIG_FILE} | jq '.| length')
index=$(($sitesCount - 1))

while [[ $index -gt -1 ]]; do
  entry=$(cat ${SITES_CONFIG_FILE} | jq ".[${index}]")

  FILENAME=$(echo $entry | jq -r .nginx_conf_filename)

  SERVER_NAME=$(echo $entry | jq -r .host)
  export SERVER_NAME=$SERVER_NAME

  APP_STORE=$(echo $entry | jq -r .app_store)
  export APP_STORE=$APP_STORE

  envsubst '${SERVER_NAME} ${APP_STORE}' < ${INPUT_TEMPLATE_FILE} > "${OUTPUT_CONFIG_DIR}_${FILENAME}.conf"

  index=$((index - 1))
done

supervisorctl start nginx
