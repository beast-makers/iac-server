#!/usr/bin/env bash
set -euxo pipefail

PHP_DISABLE_XDEBUG="${PHP_DISABLE_XDEBUG:-Yes}"

if [ "${PHP_DISABLE_XDEBUG}" != "No" ]; then
  rm -f /etc/php/7.4/fpm/conf.d/20-xdebug.ini
  rm -f /etc/php/7.4/cli/conf.d/20-xdebug.ini
fi;

supervisorctl start php7-fpm
