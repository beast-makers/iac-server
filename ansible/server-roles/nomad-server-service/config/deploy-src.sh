#!/usr/bin/env bash
set -euo pipefail

source /helper-scripts/vault-functions.sh
vault_login
vault_loadIacServerGitAccess

cd src

if [ ! -d ".git" ]; then
  git init
  git config core.sparsecheckout true
fi

echo nomad/ > .git/info/sparse-checkout
echo provision/helper-scripts >> .git/info/sparse-checkout

GIT_URL="https://${IAC_SERVER_DEPLOY_USERNAME}:${IAC_SERVER_DEPLOY_TOKEN}@gitlab.com/beast-makers/iac-server.git"

git remote -v | grep -q "origin	${GIT_URL}" \
  || git remote add -f origin ${GIT_URL} \
  || git branch --set-upstream-to=origin/master master
git pull origin master
