#!/usr/bin/env bash
set -euxo pipefail

PID_FILE='/run/haproxy/haproxy.pid'

if [ -f "$PID_FILE" ]; then
  sudo systemctl reload haproxy
fi

