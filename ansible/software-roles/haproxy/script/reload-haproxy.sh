#!/usr/bin/env bash
set -euxo pipefail

CONFIG_FILE='/etc/haproxy/haproxy.cfg'
PID_FILE='/run/haproxy/haproxy.pid'

if [ -f "$PID_FILE" ]; then
  exec haproxy -f ${CONFIG_FILE} -c -q
fi
