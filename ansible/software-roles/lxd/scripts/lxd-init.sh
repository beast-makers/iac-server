#!/usr/bin/env bash

sudo chmod 0755 /etc/profile.d/apps-bin-path.sh
. /etc/profile.d/apps-bin-path.sh
newgrp lxd
cat $1 | lxd init --preseed
