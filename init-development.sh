#!/usr/bin/env bash
set -euxo pipefail

vagrant up

vagrant ssh mastervm --command "sh /provision/development/mastervm/copy-ssh-id.sh"
vagrant ssh appvm --command "sh /provision/development/appvm/copy-ssh-id.sh"

./provision/development/mastervm/002-vagrant-run.sh
