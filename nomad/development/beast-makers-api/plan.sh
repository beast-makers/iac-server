#!/usr/bin/env bash
set -euo pipefail

source /helper-scripts/vault-functions.sh

vault_login
vault_loadDockerAuthVariables

. ./export-env.sh
export FULL_CONTAINER_IMAGE="${DOCKER_SERVER}/${DOCKER_REPOSITORY}/${DOCKER_CONTAINER_IMAGE}"
export VAULT_TOKEN=$VAULT_TOKEN

JOB_OUTPUT_FILE_PATH=/tmp/beast-makers-api.hcl

envsubst  < ../_template/hydra-webapi/hydra-webapi.tmpl.hcl > "${JOB_OUTPUT_FILE_PATH}"

cat ${JOB_OUTPUT_FILE_PATH}
nomad job plan "$@" $JOB_OUTPUT_FILE_PATH
