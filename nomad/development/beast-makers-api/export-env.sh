#!/usr/bin/env bash
set -euo pipefail

source /helper-scripts/require-docker-source-tag-env.sh

export DOCKER_SOURCE_IMAGE='beast-makers-api'
export DOCKER_CONTAINER_IMAGE="${DOCKER_SOURCE_IMAGE}:${DOCKER_SOURCE_TAG}"
export DEPLOY_JOB_NAME=beast-makers-api-job
export DEPLOY_GROUP_NAME=beast-makers-api-group
export DEPLOY_TASK_NAME=beast-makers-api-task

export SOURCE_CODE_MOUNT=/var/hydra-webapi
export SERVICE_NAME=beast-makers-api
export LB_HOST_BEGINNING=bm-api

export VAULT_ADDR=vault.service.consul:8200
export CONSUL_HTTP_ADDR=consul-server.service.consul:8500

export APP_TYPE="testing"
export APP_ENV="dev"
