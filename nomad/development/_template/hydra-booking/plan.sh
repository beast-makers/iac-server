#!/usr/bin/env bash
set -euo pipefail

source /helper-scripts/vault-functions.sh
source /helper-scripts/consul-functions.sh

vault_login
vault_loadDockerAuthVariables
vault_loadHydraWebapiDatabaseCredentials
consul_fetchDBServerConnection
consul_fetchRedisBookingMasterConnection

export HYDRA_BOOKING_SERVER_NAME=${HYDRA_BOOKING_SERVER_NAME:-"hydra-booking.local"}
DOCKER_SOURCE_IMAGE=${DOCKER_SOURCE_IMAGE:-"hydra-booking:now"}
export CONTAINER_IMAGE="${DOCKER_SERVER}/${DOCKER_REPOSITORY}/${DOCKER_SOURCE_IMAGE}"

JOB_OUTPUT_FILE_PATH=/tmp/hydra-booking-job.hcl

envsubst  < hydra-booking-job.tmpl.hcl > "${JOB_OUTPUT_FILE_PATH}"

cat ${JOB_OUTPUT_FILE_PATH}
nomad job plan "$@" $JOB_OUTPUT_FILE_PATH
