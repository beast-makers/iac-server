job "hydra-booking" {
  datacenters = ["dc1"]
  type = "service"

  update {
    max_parallel = 1
    min_healthy_time = "10s"
    healthy_deadline = "3m"
    progress_deadline = "10m"
    auto_revert = false
    canary = 0
  }

  migrate {
    max_parallel = 1
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "5m"
  }

  group "hydra-booking" {
    count = 1

    restart {
      attempts = 2
      interval = "30m"
      delay = "15s"
      mode = "fail"
    }

    task "hydra-booking" {
      driver = "docker"

      config {
        image = "${CONTAINER_IMAGE}"
        command = "/usr/bin/supervisord"
        args = [ "-n" ]
        dns_servers = ["8.8.8.8", "192.168.100.10"]

        port_map {
          http = 80
          xdebug = 9012
        }

        auth {
          username = "${DOCKER_USERNAME}"
          password = "${DOCKER_PASSWORD}"
          server_address = "${DOCKER_SERVER}/${DOCKER_REPOSITORY}"
        }

        mounts = [
          {
            type = "bind"
            source = "/var/hydra-booking"
            target = "/var/booking/"
            readonly = false
          }
        ]

        extra_hosts = [
          "${HYDRA_BOOKING_SERVER_NAME}:127.0.0.1"
        ]
      }

      env {
        HYDRA_BOOKING_SEVER_NAME = "${HYDRA_BOOKING_SERVER_NAME}"
        PHP_DISABLE_XDEBUG = "No"
        PHP_DISPLAY_ERRORS = "On"
        PHP_DISPLAY_STARTUP_ERRORS = "On"
        PHP_OPCACHE_REVALIDATE_FREQ = 0

        DB_WEBAPI_HOST = "${DB_SERVER_IP}"
        DB_WEBAPI_PORT = "${DB_SERVER_PORT}"
        DB_WEBAPI_USER = "${DB_WEBAPI_USER}"
        DB_WEBAPI_PASSWORD = "${DB_WEBAPI_PASSWORD}"

        REDIS_MASTER_HOST = "${REDIS_HYDRA_BOOKING_MASER_HOST}"
        REDIS_MASTER_PORT = "${REDIS_HYDRA_BOOKING_MASER_PORT}"
      }

      resources {
        network {
          mbits = 10
          port "http" {}
          port "xdebug" {
            static = 9012
          }
        }
      }

      service {
        name = "hydra-booking"
        port = "http"
        address_mode = "host"

        check {
          name     = "alive"
          type     = "tcp"
          interval = "10s"
          timeout  = "2s"
        }
      }
    }
  }
}
