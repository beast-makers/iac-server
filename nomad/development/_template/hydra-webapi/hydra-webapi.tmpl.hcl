job "${DEPLOY_JOB_NAME}" {
  datacenters = ["dc1"]
  type = "service"

  update {
    max_parallel = 1
    min_healthy_time = "10s"
    healthy_deadline = "3m"
    progress_deadline = "10m"
    auto_revert = false
    canary = 0
  }

  migrate {
    max_parallel = 1
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "5m"
  }

  group "${DEPLOY_GROUP_NAME}" {
    count = 1

    restart {
      attempts = 2
      interval = "30m"
      delay = "15s"
      mode = "fail"
    }

    task "${DEPLOY_TASK_NAME}" {
      driver = "docker"

      config {
        image = "${FULL_CONTAINER_IMAGE}"
        command = "/usr/bin/supervisord"
        args = [ "-n" ]
        dns_servers = ["8.8.8.8", "192.168.100.10"]

        port_map {
          http = 80
          xdebug = 9010
        }

        network_mode = "bm-network"

        auth {
          username = "${DOCKER_USERNAME}"
          password = "${DOCKER_PASSWORD}"
          server_address = "${DOCKER_SERVER}/${DOCKER_REPOSITORY}"
        }

        mounts = [
          {
            type = "bind"
            source = "${SOURCE_CODE_MOUNT}"
            target = "/var/webapi/"
            readonly = false
          }
        ]
      }

      env {
        VAULT_TMP_TOKEN = "${VAULT_TOKEN}"
        VAULT_ADDR = "${VAULT_ADDR}"
        CONSUL_HTTP_ADDR = "${CONSUL_HTTP_ADDR}"

        PHP_DISABLE_XDEBUG = "No"
        PHP_DISPLAY_ERRORS = "On"
        PHP_DISPLAY_STARTUP_ERRORS = "On"
        PHP_OPCACHE_REVALIDATE_FREQ = 0

        APP_TYPE="${APP_TYPE}"
        APP_ENV="${APP_ENV}"
      }

      resources {
        network {
          mbits = 10
          port "http" {}
          port "xdebug" {}
        }
      }

      service {
        name = "${SERVICE_NAME}"
        port = "http"
        address_mode = "host"

        meta {
          lb_host_beginning = "${LB_HOST_BEGINNING}"
        }

        check {
          type     = "tcp"
          port     = "http"
          interval = "10s"
          timeout  = "2s"
        }
      }
    }
  }
}
