#!/usr/bin/env bash
set -euo pipefail

source /helper-scripts/require-docker-source-tag-env.sh

export DOCKER_SOURCE_IMAGE='customer-webportal'
export DOCKER_CONTAINER_IMAGE="${DOCKER_SOURCE_IMAGE}:${DOCKER_SOURCE_TAG}"
export DEPLOY_JOB_NAME=customer-webportal-job
export DEPLOY_GROUP_NAME=customer-webportal-group
export DEPLOY_TASK_NAME=customer-webportal-task

export SOURCE_CODE_MOUNT=/var/hydra-webfe
export SERVICE_NAME=customer-webportal
export LB_HOST_BEGINNING=customer-webportal

export VAULT_ADDR=vault.service.consul:8200
export CONSUL_HTTP_ADDR=consul-server.service.consul:8500

export APP_TYPE="testing"
export APP_ENV="dev"
