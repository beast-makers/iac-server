#!/usr/bin/env bash

DATABASE_NAME=${DATABASE_NAME:-"beast_makers"}

source /provision/helper-scripts/require-vault-env.sh
source /provision/helper-scripts/vault-functions.sh

vault_login
vault_loadHydraWebapiDatabaseCredentials

COMMAND=$1
shift

terraform $COMMAND \
  -var "consul-server-ip=${CONSUL_SERVER_IP}" \
  -var "database-name=${DATABASE_NAME}" \
  -var "database-user=${DB_WEBAPI_USER}" \
  -var "database-password=${DB_WEBAPI_PASSWORD}" \
  "$@"
