variable "image-tag" {
  default = "now"
}

variable "consul-server-ip" {}
variable "database-name" {}
variable "database-user" {}
variable "database-password" {}

terraform {
  required_providers {
    lxd = {
      source = "arren-ru/lxd"
      version = "1.3.1"
    }
  }
}

provider "lxd" {
  generate_client_certificates = true
  accept_remote_certificate    = true
}

resource "lxd_container" "database-service-master" {
  count = 1
  name = format("database-service-master-%02d", count.index)
  image = "database-service-base__${var.image-tag}"
  ephemeral = false

  config = {
    "boot.autostart" = true
  }

  provisioner "local-exec" {
    command = "printf -- \"${templatefile("inventory.tpl", {container_name = self.name})}\" > ./inventory/${self.name}-inventory.yml"
  }

  provisioner "local-exec" {
    command = <<CMD
        sh -c " \
          cd /ansible \
          && ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook \
            -i ${path.cwd}/inventory/${self.name}-inventory.yml \
            --extra-vars \"consul_service_id=${self.name}-${uuid()}\" \
            --extra-vars \"consul_server_ip=${var.consul-server-ip}\" \
            --extra-vars \"consul_serf_port=8306\" \
            --extra-vars \"consul_http_port=8506\" \
            --extra-vars \"consul_rpc_port=8426\" \
            --extra-vars \"consul_node_name=consul-client-${self.name}-master\" \
            --extra-vars \"database_name=${var.database-name} database_user=${var.database-user} database_password=${var.database-password}\" \
            --tags database-service-master\
            database-service.yml \
        "
CMD
  }
}
