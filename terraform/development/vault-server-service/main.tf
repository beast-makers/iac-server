variable "image-tag" {
  default = "now"
}
variable "consul-server-ip" {}
variable "vault-api-port" {}

terraform {
  required_providers {
    lxd = {
      source = "arren-ru/lxd"
      version = "1.3.1"
    }
  }
}

provider "lxd" {
  generate_client_certificates = true
  accept_remote_certificate    = true
}

resource "lxd_container" "vault-server-service" {
  count = 1
  name = format("vault-server-service-%02d", count.index)
  image = "vault-server-service-base__${var.image-tag}"
  ephemeral = false

  config = {
    "boot.autostart" = true
    "security.privileged" = true
  }

  provisioner "local-exec" {
    command = "printf -- \"${templatefile("inventory.tpl", {container_name = self.name})}\" > ./inventory/${self.name}-inventory.yml"
  }

  provisioner "local-exec" {
    command = <<CMD
        sh -c " \
          cd /ansible \
          && ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook \
            -i ${path.cwd}/inventory/${self.name}-inventory.yml \
            --extra-vars \"consul_service_id=${self.name}-${uuid()}\" \
            --extra-vars \"consul_server_ip=${var.consul-server-ip}\" \
            --extra-vars \"consul_serf_port=8305\" \
            --extra-vars \"consul_http_port=8505\" \
            --extra-vars \"consul_rpc_port=8425\" \
            --extra-vars \"consul_node_name=consul-client-${self.name}\" \
            --extra-vars \"vault_api_port=${var.vault-api-port}\" \
            --skip-tags install \
            vault-server-service.yml \
        "
CMD
  }

  provisioner "local-exec" {
    command = <<CMD
        grep -q VAULT_ADDR /etc/environment || echo 'VAULT_ADDR="${self.ip_address}:8200"' | sudo tee -a /etc/environment;
        grep -q VAULT_ADDR /etc/environment && sudo sed -i -e "s|VAULT_ADDR=\".*\"|VAULT_ADDR=\"${self.ip_address}:${var.vault-api-port}\"|" /etc/environment;
CMD
  }
}

