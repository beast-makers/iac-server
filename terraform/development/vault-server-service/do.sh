#!/usr/bin/env bash

VAULT_API_PORT=${VAULT_API_PORT:-8200}

COMMAND=$1
shift

terraform $COMMAND \
  -var "consul-server-ip=${CONSUL_SERVER_IP}" \
  -var "vault-api-port=${VAULT_API_PORT}" \
  "$@"
