#!/usr/bin/env bash

REDIS_PORT=${REDIS_PORT:-"6381"}

COMMAND=$1
shift

terraform $COMMAND \
  -var "consul-server-ip=${CONSUL_SERVER_IP}" \
  -var "redis-port=${REDIS_PORT}" \
  "$@"
