#!/usr/bin/env bash

source ../../../provision/helper-scripts/require-vault-env.sh
source ../../../provision/helper-scripts/vault-functions.sh

vault_login

COMMAND=$1
shift

NOMAD_RPC_ADDRESS='192.168.100.10'

terraform $COMMAND \
  -var "consul-server-ip=${CONSUL_SERVER_IP}" \
  -var "nomad-address-rpc=${NOMAD_RPC_ADDRESS}" \
  "$@"
