variable "image-tag" {
  default = "now"
}

variable "consul-server-ip" {}
variable "nomad-address-rpc" {}

terraform {
  required_providers {
    lxd = {
      source = "arren-ru/lxd"
      version = "1.3.1"
    }
  }
}

provider "consul" {
//  address    = "xxx" This should be read from env var CONSUL_HTTP_ADDR
  scheme = "http"
}

data "consul_service" "read-vault" {
  name = "vault"
  datacenter = "dc1"
  tag = "vault"
}

provider "lxd" {
  generate_client_certificates = true
  accept_remote_certificate    = true
}

resource "lxd_container" "nomad-server-service" {
  count = 1
  name = format("nomad-server-service-%02d", count.index)
  image = "nomad-server-service-base__${var.image-tag}"
  ephemeral = false

  config = {
    "boot.autostart" = true
  }

  provisioner "local-exec" {
    command = "printf -- \"${templatefile("inventory.tpl", {container_name = self.name})}\" > ./inventory/${self.name}-inventory.yml"
  }

  provisioner "local-exec" {
    command = <<CMD
        sh -c " \
          cd /ansible \
          && ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook \
            -i ${path.cwd}/inventory/${self.name}-inventory.yml \
            --extra-vars \"consul_service_id=${self.name}-${uuid()}\" \
            --extra-vars \"consul_server_ip=${var.consul-server-ip}\" \
            --extra-vars \"consul_serf_port=8304\" \
            --extra-vars \"consul_http_port=8504\" \
            --extra-vars \"consul_rpc_port=8424\" \
            --extra-vars \"nomad_advertise_rpc=${var.nomad-address-rpc}\" \
            --extra-vars \"consul_node_name=consul-client-${self.name}\" \
            --extra-vars \"vault_ip=${data.consul_service.read-vault.service[0].address}\" \
            --extra-vars \"vault_api_port=${data.consul_service.read-vault.service[0].port}\" \
            --skip-tags install \
            nomad-server-service.yml \
        "
CMD
  }

  provisioner "local-exec" {
    command = <<CMD
      lxc config device remove ${self.name} ${self.name}_jobs;
      lxc config device add ${self.name} ${self.name}_jobs disk source=/nomad path=/nomad;
      lxc config device remove ${self.name} ${self.name}_helper_scripts;
      lxc config device add ${self.name} ${self.name}_helper_scripts disk source=/provision/helper-scripts path=/helper-scripts;
CMD
  }
}
