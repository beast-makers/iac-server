---
all:
  hosts:
    ${container_name}:
      ansible_host: ${container_name}
      ansible_connection: lxd
