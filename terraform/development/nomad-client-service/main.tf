variable "nomad-server-ip" {}
variable "nomad-advertise-http" {}
variable "nomad-advertise-rpc" {}
variable "nomad-advertise-serf" {}
variable "appvm-server-ip" {}
variable "consul-server-ip" {}
variable "consul-advertise-ip" {}
variable "ssh-user" {}
variable "name" {
  default = "nomad-client-service-00"
}

resource "null_resource" "nomad-client-service" {
  count = 1

  triggers = {
    force_reprovision = uuid()
  }

  provisioner "local-exec" {
    command = "printf -- \"${templatefile("inventory.tpl", {server_host_name = var.name, server_ip = var.appvm-server-ip, ssh_user = var.ssh-user})}\" > ./inventory/${var.name}-inventory.yml"
  }

  provisioner "local-exec" {
    command = <<CMD
        sh -c " \
          cd /ansible \
          && ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook \
            -i ${path.cwd}/inventory/${var.name}-inventory.yml \
            --extra-vars \"consul_client_ip=${var.consul-server-ip}\" \
            --extra-vars \"consul_http_port=8500\" \
            app-server.yml \
        "
CMD
  }

  provisioner "local-exec" {
    command = <<CMD
        sh -c " \
          cd /ansible \
          && ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook \
            -i ${path.cwd}/inventory/${var.name}-inventory.yml \
            --extra-vars \"nomad_server_ip=${var.nomad-server-ip}\" \
            --extra-vars \"nomad_advertise_http=${var.nomad-advertise-http}\" \
            --extra-vars \"nomad_advertise_rpc=${var.nomad-advertise-rpc}\" \
            --extra-vars \"nomad_advertise_serf=${var.nomad-advertise-serf}\" \
            --extra-vars \"consul_service_id=${var.name}-${uuid()}\" \
            --extra-vars \"consul_server_ip=${var.consul-server-ip}\" \
            --extra-vars \"consul_advertise_ip=${var.consul-advertise-ip}\" \
            --extra-vars \"consul_node_name=consul-client-${var.name}\" \
            nomad-client-service.yml \
        "
CMD
  }
}
