#!/usr/bin/env bash

COMMAND=$1
shift

APPVM_SERVER_IP='192.168.100.11'
CONSUL_SERVER_IP='192.168.100.10'
NOMAD_SERVER_IP='192.168.100.10'
SSH_USER='vagrant'

terraform $COMMAND \
  -var "nomad-server-ip=${NOMAD_SERVER_IP}" \
  -var "nomad-advertise-http=${APPVM_SERVER_IP}" \
  -var "nomad-advertise-rpc=${APPVM_SERVER_IP}" \
  -var "nomad-advertise-serf=${APPVM_SERVER_IP}" \
  -var "consul-server-ip=${CONSUL_SERVER_IP}" \
  -var "consul-advertise-ip=${APPVM_SERVER_IP}" \
  -var "appvm-server-ip=${APPVM_SERVER_IP}" \
  -var "ssh-user=${SSH_USER}" \
  "$@"
