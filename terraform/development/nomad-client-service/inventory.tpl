---
all:
  hosts:
    ${server_host_name}:
      ansible_host: ${server_ip}
      ansible_connection: ssh
      ansible_user: ${ssh_user}
