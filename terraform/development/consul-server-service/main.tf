variable "image-tag" {
  default = "now"
}

terraform {
  required_providers {
    lxd = {
      source = "arren-ru/lxd"
      version = "1.3.1"
    }
  }
}

provider "lxd" {
  generate_client_certificates = true
  accept_remote_certificate    = true
}

resource "lxd_container" "consul-server-service" {
  count = 1
  name = format("consul-server-service-%02d", count.index)
  image = "consul-server-service-base__${var.image-tag}"
  ephemeral = false

  config = {
    "boot.autostart" = true
    "boot.autostart.delay" = 3
  }

  provisioner "local-exec" {
    command = "printf -- \"${templatefile("inventory.tpl", {container_name = self.name})}\" > ./inventory/${self.name}-inventory.yml"
  }

  provisioner "local-exec" {
    command = <<CMD
        sh -c " \
          cd /ansible \
          && ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook \
            -i ${path.cwd}/inventory/${self.name}-inventory.yml \
            --extra-vars \"consul_service_id=${self.name}-${uuid()}\" \
            --extra-vars \"consul_client_ip=${lxd_container.consul-server-service.0.ip_address}\" \
            --skip-tags install \
            consul-server-service.yml \
        "
CMD
  }

  provisioner "local-exec" {
    command = <<CMD
        grep -q CONSUL_HTTP_ADDR /etc/environment || echo 'CONSUL_HTTP_ADDR="${lxd_container.consul-server-service.0.ip_address}:8500"' | sudo tee -a /etc/environment;
        grep -q CONSUL_HTTP_ADDR /etc/environment && sudo sed -i -e "s|CONSUL_HTTP_ADDR=\".*\"|CONSUL_HTTP_ADDR=\"${lxd_container.consul-server-service.0.ip_address}:8500\"|" /etc/environment;
        grep -q CONSUL_SERVER_IP /etc/environment || echo 'CONSUL_SERVER_IP="${lxd_container.consul-server-service.0.ip_address}"' | sudo tee -a /etc/environment;
        grep -q CONSUL_SERVER_IP /etc/environment && sudo sed -i -e "s|CONSUL_SERVER_IP=\".*\"|CONSUL_SERVER_IP=\"${lxd_container.consul-server-service.0.ip_address}\"|" /etc/environment;
        sudo sed -i -e "s|address = \".*\"|address = \"${lxd_container.consul-server-service.0.ip_address}:8500\"|" /etc/consul-template/consul-template.hcl;
CMD
  }

  provisioner "local-exec" {
    command = "sudo systemctl restart consul-template"
  }

  provisioner "local-exec" {
    command = "printf -- \"consul servers [${join(",", lxd_container.consul-server-service.*.ip_address)}]\""
  }
}
