#!/usr/bin/env bash

COMMAND=$1
shift

terraform $COMMAND "$@"
