variable "image-tag" {
  default = "now"
}
variable "consul-server-ip" {}

terraform {
  required_providers {
    lxd = {
      source = "arren-ru/lxd"
      version = "1.3.1"
    }
  }
}

provider "lxd" {
  generate_client_certificates = true
  accept_remote_certificate    = true
}

resource "lxd_container" "haproxy-service" {
  count = 1
  name = format("haproxy-service-%02d", count.index)
  image = "haproxy-service-base__${var.image-tag}"
  ephemeral = false

  config = {
    "boot.autostart" = true
  }

  provisioner "local-exec" {
    command = "printf -- \"${templatefile("inventory.tpl", {container_name = self.name})}\" > ./inventory/${self.name}-inventory.yml"
  }

  provisioner "local-exec" {
    command = <<CMD
        sh -c " \
          cd /ansible \
          && ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook \
            -i ${path.cwd}/inventory/${self.name}-inventory.yml \
            --extra-vars \"consul_service_id=${self.name}-${uuid()}\" \
            --extra-vars \"consul_server_ip=${var.consul-server-ip}\" \
            --extra-vars \"consul_serf_port=8303\" \
            --extra-vars \"consul_http_port=8503\" \
            --extra-vars \"consul_rpc_port=8423\" \
            --extra-vars \"consul_node_name=consul-client-${self.name}\" \
            --skip-tags install \
            haproxy-service.yml \
        "
CMD
  }
}
