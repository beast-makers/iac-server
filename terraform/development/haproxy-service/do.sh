#!/usr/bin/env bash

COMMAND=$1
shift

terraform $COMMAND \
  -var "consul-server-ip=${CONSUL_SERVER_IP}" \
  "$@"
