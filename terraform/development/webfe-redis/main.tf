variable "image-tag" {
  default = "now"
}

variable "consul-server-ip" {}
variable "redis-port" {}

terraform {
  required_providers {
    lxd = {
      source = "arren-ru/lxd"
      version = "1.3.1"
    }
  }
}

provider "lxd" {
  generate_client_certificates = true
  accept_remote_certificate    = true
}

resource "lxd_container" "webfe-redis" {
  count = 1
  name = format("webfe-redis-%02d", count.index)
  image = "webfe-redis-base__${var.image-tag}"
  ephemeral = false

  config = {
    "boot.autostart" = true
  }

  provisioner "local-exec" {
    command = "printf -- \"${templatefile("inventory.tpl", {container_name = self.name})}\" > ./inventory/${self.name}-inventory.yml"
  }

  provisioner "local-exec" {
    command = <<CMD
        sh -c " \
          cd /ansible \
          && ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook \
            -i ${path.cwd}/inventory/${self.name}-inventory.yml \
            --extra-vars \"consul_service_id=${self.name}-${uuid()}\" \
            --extra-vars \"consul_server_ip=${var.consul-server-ip}\" \
            --extra-vars \"consul_serf_port=8307\" \
            --extra-vars \"consul_http_port=8507\" \
            --extra-vars \"consul_rpc_port=8427\" \
            --extra-vars \"redis_port=${var.redis-port}\" \
            --extra-vars \"consul_node_name=consul-client-${self.name}\" \
            --skip-tags install \
            webfe-redis.yml \
        "
CMD
  }
}
