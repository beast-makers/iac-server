variables {
  target-tag = "now"
  target-image = "local-dns-base"
  source-image = "images:debian/10"
}

locals {
  target-name = "${var.target-image}__${var.target-tag}"
}

source "lxd" "local-dns" {
  image = var.source-image
  output_image = local.target-name
  publish_properties = {
    description = "local dns base image"
  }
}

build {
  name = "local dns base lxd image"

  source "source.lxd.local-dns" {}

  provisioner "shell" {
    inline = [
      "apt update",
      "apt install python -y"
    ]
  }

  provisioner "ansible" {
    user = "root"
    playbook_file = "/ansible/local-dns.yml"
    ansible_env_vars = [
      "ANSIBLE_CONFIG=/ansible/ansible.cfg"
    ]
    extra_arguments = [
      "--tags", "install",
      "--extra-vars", "ansible_host=packer-local-dns ansible_connection=lxd"
    ]
  }
}
