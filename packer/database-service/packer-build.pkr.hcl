variables {
  target-tag = "now"
  target-image = "database-service-base"
  source-image = "images:debian/10"
}

locals {
  target-name = "${var.target-image}__${var.target-tag}"
}

source "lxd" "database-service" {
  image = var.source-image
  output_image = local.target-name
  publish_properties = {
    description = "database service base image"
  }
}

build {
  name = "database service base lxd image"

  source "source.lxd.database-service" {}

  provisioner "shell" {
    inline = [
      "apt update",
      "apt install python -y"
    ]
  }

  provisioner "ansible" {
    user = "root"
    playbook_file = "/ansible/database-service.yml"
    ansible_env_vars = [
      "ANSIBLE_CONFIG=/ansible/ansible.cfg"
    ]
    extra_arguments = [
      "--tags", "install",
      "--extra-vars", "ansible_host=packer-database-service ansible_connection=lxd"
    ]
  }
}
