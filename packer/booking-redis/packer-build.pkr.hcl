variables {
  target-tag = "now"
  target-image = "booking-redis-base"
  source-image = "images:debian/10"
}

locals {
  target-name = "${var.target-image}__${var.target-tag}"
}

source "lxd" "booking-redis" {
  image = var.source-image
  output_image = local.target-name
  publish_properties = {
    description = "booking-redis base image"
  }
}

build {
  name = "booking-redis base lxd image"

  source "source.lxd.booking-redis" {}

  provisioner "shell" {
    inline = [
      "apt update",
      "apt install python -y"
    ]
  }

  provisioner "ansible" {
    user = "root"
    playbook_file = "/ansible/booking-redis.yml"
    ansible_env_vars = [
      "ANSIBLE_CONFIG=/ansible/ansible.cfg"
    ]
    extra_arguments = [
      "--tags", "install",
      "--extra-vars", "ansible_host=packer-booking-redis ansible_connection=lxd"
    ]
  }
}
