variables {
  target-tag = "now"
  target-image = "haproxy-service-base"
  source-image = "images:debian/10"
}

locals {
  target-name = "${var.target-image}__${var.target-tag}"
}

source "lxd" "haproxy-service" {
  image = var.source-image
  output_image = local.target-name
  publish_properties = {
    description = "haproxy service base image"
  }
}

build {
  name = "haproxy service base lxd image"

  source "source.lxd.haproxy-service" {}

  provisioner "shell" {
    inline = [
      "apt update",
      "apt install python -y"
    ]
  }

  provisioner "ansible" {
    user = "root"
    playbook_file = "/ansible/haproxy-service.yml"
    ansible_env_vars = [
      "ANSIBLE_CONFIG=/ansible/ansible.cfg"
    ]
    extra_arguments = [
      "--tags", "install",
      "--extra-vars", "ansible_host=packer-haproxy-service ansible_connection=lxd"
    ]
  }
}
