variables {
  target-tag = "now"
  target-image = "webfe-redis-base"
  source-image = "images:debian/10"
}

locals {
  target-name = "${var.target-image}__${var.target-tag}"
}

source "lxd" "webfe-redis" {
  image = var.source-image
  output_image = local.target-name
  publish_properties = {
    description = "webfe-redis base image"
  }
}

build {
  name = "webfe-redis base lxd image"

  source "source.lxd.webfe-redis" {}

  provisioner "shell" {
    inline = [
      "apt update",
      "apt install python -y"
    ]
  }

  provisioner "ansible" {
    user = "root"
    playbook_file = "/ansible/webfe-redis.yml"
    ansible_env_vars = [
      "ANSIBLE_CONFIG=/ansible/ansible.cfg"
    ]
    extra_arguments = [
      "--tags", "install",
      "--extra-vars", "ansible_host=packer-webfe-redis ansible_connection=lxd"
    ]
  }
}
