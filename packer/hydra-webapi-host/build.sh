#!/usr/bin/env bash

source ../../provision/helper-scripts/require-target-tag-env.sh
source ../../provision/helper-scripts/require-vault-env.sh
source ../../provision/helper-scripts/vault-functions.sh

vault_login
vault_loadDockerAuthVariables

packer build \
  -var="target-tag=${TARGET_TAG}" \
  -var="docker-server=${DOCKER_SERVER}" \
  -var="docker-repo=${DOCKER_REPOSITORY}" \
  -var="docker-username=${DOCKER_USERNAME}" \
  -var="docker-password=${DOCKER_PASSWORD}" \
  ./packer-build.pkr.hcl "$@"
