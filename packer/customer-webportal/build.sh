#!/usr/bin/env bash

source ../../provision/helper-scripts/require-source-tag-env.sh
source ../../provision/helper-scripts/require-target-tag-env.sh
source ../../provision/helper-scripts/require-vault-env.sh
source ../../provision/helper-scripts/vault-functions.sh

vault_login
vault_loadCustomerWebPortalAccessToken
vault_loadDockerAuthVariables
source ../../provision/helper-scripts/require-git-access-token-env.sh

packer build \
  -var="source-tag=${SOURCE_TAG}" \
  -var="target-tag=${TARGET_TAG}" \
  -var="git-access-token=${GIT_ACCESS_TOKEN}" \
  -var="docker-server=${DOCKER_SERVER}" \
  -var="docker-repo=${DOCKER_REPOSITORY}" \
  -var="docker-username=${DOCKER_USERNAME}" \
  -var="docker-password=${DOCKER_PASSWORD}" \
  ./packer-build.pkr.hcl "$@"
