variables {
  target-tag = "now"
  target-image = "customer-webportal"
  source-image = "hydra-webfe-host"
  source-tag = "now"

  docker-server = "xxx"
  docker-repo = "xxx"
}

variable "docker-username" {
  default   = "xxx"
  sensitive = true
}

variable "docker-password" {
  default   = "xxx"
  sensitive = true
}

variable "git-access-token" {
  default   = "xxx"
  sensitive = true
}

locals {
  target-repo = "${var.docker-server}/${var.docker-repo}"
  source-repo = "${var.docker-server}/${var.docker-repo}"
}

source "docker" "customer-webportal" {
  image = "${local.target-repo}/${var.source-image}:${var.source-tag}"
  commit = true
  login = true
  login_password = var.docker-password
  login_server = local.source-repo
  login_username = var.docker-username
  run_command = ["-d", "-i", "-t",
    "--name", "${var.target-image}",
    "{{.Image}}",
    "/bin/bash"
  ]
}

build {
  name = "customer-webportal docker image"

  source "source.docker.customer-webportal" {}

  provisioner "shell" {
    inline = [
      "apt update",
      "apt install python -y"
    ]
  }

  provisioner "ansible" {
    user = "root"
    playbook_file = "/ansible/customer-webportal.yml"
    ansible_env_vars = [
      "ANSIBLE_CONFIG=/ansible/ansible.cfg"
    ]
    extra_arguments = [
      "--extra-vars", "ansible_host=${var.target-image} ansible_connection=docker",
      "--extra-vars", "git_access_token=${var.git-access-token}"
    ]
  }

  post-processors {
    post-processor "docker-tag" {
      repository = "${local.target-repo}/${var.target-image}"
      tag = [ var.target-tag ]
    }
    post-processor "docker-push" {
      login = true
      login_password = var.docker-password
      login_server = var.docker-server
      login_username = var.docker-username
    }
  }
}
