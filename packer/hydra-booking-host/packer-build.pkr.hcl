variables {
  target-tag = "now"
  target-image = "hydra-booking-host"
  source-image = "debian:buster"

  docker-server = "xxx"
  docker-repo = "xxx"
}

variable "docker-username" {
  default   = "xxx"
  sensitive = true
}

variable "docker-password" {
  default   = "xxx"
  sensitive = true
}

locals {
  target-repo = "${var.docker-server}/${var.docker-repo}"
}

source "docker" "hydra-booking-host" {
  image = "debian:buster"
  commit = true
  run_command = ["-d", "-i", "-t",
    "--name", "${var.target-image}",
    "{{.Image}}",
    "/bin/bash"
  ]
}

build {
  name = "hydra-booking base host docker image"

  source "source.docker.hydra-booking-host" {}

  provisioner "shell" {
    inline = [
      "apt update",
      "apt install python -y"
    ]
  }

  provisioner "ansible" {
    user = "root"
    playbook_file = "/ansible/hydra-booking-host.yml"
    ansible_env_vars = [
      "ANSIBLE_CONFIG=/ansible/ansible.cfg"
    ]
    extra_arguments = [
      "--extra-vars", "ansible_host=${var.target-image} ansible_connection=docker"
    ]
  }

  post-processors {
    post-processor "docker-tag" {
      repository = "${local.target-repo}/${var.target-image}"
      tags = [ var.target-tag ]
    }
    post-processor "docker-push" {
      login = true
      login_password = var.docker-password
      login_server = var.docker-server
      login_username = var.docker-username
    }
  }
}
