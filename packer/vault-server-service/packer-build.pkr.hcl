variables {
  target-tag = "now"
  target-image = "vault-server-service-base"
  source-image = "images:debian/10"
}

locals {
  target-name = "${var.target-image}__${var.target-tag}"
}

source "lxd" "vault-server-service" {
  image = var.source-image
  output_image = local.target-name
  publish_properties = {
    description = "hashicorp vault server service base image"
  }
}

build {
  name = "hashicorp vault-server-service base lxd image"

  source "source.lxd.vault-server-service" {}

  provisioner "shell" {
    inline = [
      "apt update",
      "apt install python -y"
    ]
  }

  provisioner "ansible" {
    user = "root"
    playbook_file = "/ansible/vault-server-service.yml"
    ansible_env_vars = [
      "ANSIBLE_CONFIG=/ansible/ansible.cfg"
    ]
    extra_arguments = [
      "--tags", "install",
      "--extra-vars", "ansible_host=packer-vault-server-service ansible_connection=lxd"
    ]
  }
}
