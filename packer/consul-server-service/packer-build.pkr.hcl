variables {
  target-tag = "now"
  target-image = "consul-server-service-base"
  source-image = "images:debian/10"
}

locals {
  target-name = "${var.target-image}__${var.target-tag}"
}

source "lxd" "consul-server-service" {
  image = var.source-image
  output_image = local.target-name
  publish_properties = {
    description = "hashicorp consul-server-service base image"
  }
}

build {
  name = "hashicorp consul-server-service base lxd image"

  source "source.lxd.consul-server-service" {}

  provisioner "shell" {
    inline = [
      "apt update",
      "apt install python -y"
    ]
  }

  provisioner "ansible" {
    user = "root"
    playbook_file = "/ansible/consul-server-service.yml"
    ansible_env_vars = [
      "ANSIBLE_CONFIG=/ansible/ansible.cfg"
    ]
    extra_arguments = [
      "--tags", "install",
      "--extra-vars", "ansible_host=packer-consul-server-service ansible_connection=lxd"
    ]
  }
}
