variables {
  target-tag = "now"
  target-image = "nomad-server-service-base"
  source-image = "images:debian/10"
}

locals {
  target-name = "${var.target-image}__${var.target-tag}"
}

source "lxd" "nomad-server-service" {
  image = var.source-image
  output_image = local.target-name
  publish_properties = {
    description = "hashicorp nomad server service base image"
  }
}

build {
  name = "hashicorp nomad server service base lxd image"

  source "source.lxd.nomad-server-service" {}

  provisioner "shell" {
    inline = [
      "apt update",
      "apt install python -y"
    ]
  }

  provisioner "ansible" {
    user = "root"
    playbook_file = "/ansible/nomad-server-service.yml"
    ansible_env_vars = [
      "ANSIBLE_CONFIG=/ansible/ansible.cfg"
    ]
    extra_arguments = [
      "--tags", "install",
      "--extra-vars", "ansible_host=packer-nomad-server-service ansible_connection=lxd"
    ]
  }
}
