variables {
  target-tag = "now"
  target-image = "webapi-redis-base"
  source-image = "images:debian/10"
}

locals {
  target-name = "${var.target-image}__${var.target-tag}"
}

source "lxd" "webapi-redis" {
  image = var.source-image
  output_image = local.target-name
  publish_properties = {
    description = "webapi-redis base image"
  }
}

build {
  name = "webapi-redis base lxd image"

  source "source.lxd.webapi-redis" {}

  provisioner "shell" {
    inline = [
      "apt update",
      "apt install python -y"
    ]
  }

  provisioner "ansible" {
    user = "root"
    playbook_file = "/ansible/webapi-redis.yml"
    ansible_env_vars = [
      "ANSIBLE_CONFIG=/ansible/ansible.cfg"
    ]
    extra_arguments = [
      "--tags", "install",
      "--extra-vars", "ansible_host=packer-webapi-redis ansible_connection=lxd"
    ]
  }
}
